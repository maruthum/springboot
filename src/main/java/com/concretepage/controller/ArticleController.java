package com.concretepage.controller;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.concretepage.Rules.CabRules;
import com.concretepage.entity.*;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.easyrules.api.RulesEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMethod;

import com.concretepage.service.IArticleService;

import static org.easyrules.core.RulesEngineBuilder.aNewRulesEngine;

@Controller
@RequestMapping(value="/user")
@CrossOrigin(origins = {"http://localhost:4200"},allowCredentials = "false")
//@CrossOrigin(origins = {"http://localhost:2222"},allowCredentials = "false")
public class ArticleController {
	
	@Autowired
	private IArticleService articleService;
	@GetMapping("article")
	
	//mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
	
	public ResponseEntity<Article> getArticleById(@RequestParam("id") String id) {
		Article article = articleService.getArticleById(Integer.parseInt(id));
		return new ResponseEntity<Article>(article, HttpStatus.OK);
	}
	
	/*	@PostMapping("login")
public ResponseEntity<Void> handle() {
		System.out.println("HI test");
		   HttpHeaders responseHeaders = new HttpHeaders();
		   responseHeaders.set("MyResponseHeader", "MyValue");
		   return new ResponseEntity<Void>(responseHeaders, HttpStatus.CREATED);
		 }*/
	/*public ResponseEntity<List<Article>> greetingSubmit(@RequestParam("email_id") String email ,@RequestParam("pwd") String pass) {
		System.out.println("testprint "+email);
		System.out.println("testprint "+pass);
		List<Article> list =  articleService.login(email,pass);
		return new ResponseEntity<List<Article>>(list, HttpStatus.OK);
      // return new ResponseEntity<String>("got result"+email,HttpStatus.CREATED);
		
	}	*/	
	
	@RequestMapping(value = "/login_validations", method = RequestMethod.POST)
	public ResponseEntity<Void> login(@RequestBody Login_Validation obj, UriComponentsBuilder builder) {
		System.out.println("Email : "+obj.getEmail());
		System.out.println("password : "+obj.getPassword());
		boolean flag = articleService.Login_Validation(obj);
		//System.out.println("Flag result"+flag);
		//List<Article> list =  articleService.login( greeting );
		System.out.println(HttpStatus.OK);
		return new ResponseEntity<Void>( HttpStatus.OK);
      // return new ResponseEntity<String>("got result"+email,HttpStatus.CREATED);
		
	}	
	
	@RequestMapping(value = "/easyrules", method = RequestMethod.POST)
	public ResponseEntity<Void> easyrules(@RequestBody CabMaster cabmaster, UriComponentsBuilder builder) {
		System.out.println("CabNo : "+cabmaster.getCab_no());
		System.out.println("CabModel : "+cabmaster.getCab_model());
		System.out.println("Capacity : "+cabmaster.getCapacity());
		System.out.println("TariffType : "+cabmaster.getTariff_type());
		System.out.println("TripAmount : "+cabmaster.getTrip_amount());
		System.out.println("InTime : "+cabmaster.getIn_time());
		System.out.println("OutTime : "+cabmaster.getOut_time());

		boolean flag = articleService.easyrules(cabmaster);

		return new ResponseEntity<Void>( HttpStatus.OK);
//		System.out.println("Flag result"+flag);
		//List<Article> list =  articleService.login( greeting );

      // return new ResponseEntity<String>("got result"+email,HttpStatus.CREATED);
		
	}

	@RequestMapping(value = "/rulesmaster", method = RequestMethod.POST)
	public ResponseEntity<Void> rulesmaster(@RequestBody RulesMaster rulesmaster, UriComponentsBuilder builder) {
		System.out.println("Rule Name : "+rulesmaster.getRule_name());
		System.out.println("Rule Value : "+rulesmaster.getRule_value());
		System.out.println("Category : "+rulesmaster.getRule_category());
		System.out.println("Preference : "+rulesmaster.getPreference());

		boolean flag = articleService.rulesmaster(rulesmaster);

		return new ResponseEntity<Void>( HttpStatus.OK);
//		System.out.println("Flag result"+flag);
		//List<Article> list =  articleService.login( greeting );

      // return new ResponseEntity<String>("got result"+email,HttpStatus.CREATED);

	}
	@GetMapping("startrule")
	public void StartRuleEngine() {
		System.out.println("Start Rule Engine");
		String getmodel;
		List<CabMaster> list = articleService.StartRuleEngine();
		List<RulesMaster> rules = articleService.GetRulesMaster();
		CabRules cabrules = new CabRules();
		RulesEngine rulesEngine = aNewRulesEngine().build();
		for(CabMaster b:list){
			for(RulesMaster r:rules) {
				cabrules.setCabdetail(b);
				cabrules.setRulesdetails(r);
				rulesEngine.registerRule(cabrules);
				rulesEngine.fireRules();
				if (cabrules.getCabdetail() != null) {
					getmodel = cabrules.getCabdetail().getCab_model();
					System.out.println(getmodel);
				}
			}
		}
//		return new ResponseEntity<List<RulesMaster>>(rules, HttpStatus.OK);
	}
	  
	/*public ResponseEntity<Greeting> (@RequestBody Greeting greeting) {
		System.out.println("formsubmited");
		
		
	}*/
	/*public String greetingSubmit(@ModelAttribute Greeting greeting) {
		System.out.println("Get TEST");
        return "result";
    }*/
	/*public String greetingSubmit(@RequestParam("email") String email ) {
		System.out.println("testprint "+email);
        return "Test"+ email;
    }*/
	@GetMapping("all-articles")
	public ResponseEntity<List<Article>> getAllArticles() {
		List<Article> list = articleService.getAllArticles();
		return new ResponseEntity<List<Article>>(list, HttpStatus.OK);
	}
	@RequestMapping("getAllUsers")
	public ResponseEntity<List<Greeting>> getAllUsers() {
		System.out.println("Got user list");
		List<Greeting> list = articleService.getAllUsers();
		return new ResponseEntity<List<Greeting>>(list, HttpStatus.OK);
	}
	@PostMapping("/article")
	public ResponseEntity<Void> createArticle(@RequestBody Article article, UriComponentsBuilder builder) {
		boolean flag = articleService.createArticle(article);
        if (flag == false) {
        	return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/article?id={id}").buildAndExpand(article.getArticleId()).toUri());
        //System.out.println("WELCOME TEST 2"+article.getArticleId());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}
	@PutMapping("article")
	public ResponseEntity<Article> updateArticle(@RequestBody Article article) {
		articleService.updateArticle(article);
		return new ResponseEntity<Article>(article, HttpStatus.OK);
	}
	@DeleteMapping("article")
	public ResponseEntity<Void> deleteArticle(@RequestParam("id") String id) {
		articleService.deleteArticle(Integer.parseInt(id));
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}	
} 