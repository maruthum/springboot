package com.concretepage.Rules;

import com.concretepage.entity.CabMaster;
import com.concretepage.entity.RulesMaster;
import org.easyrules.annotation.Action;
import org.easyrules.annotation.Condition;
import org.easyrules.annotation.Rule;

import java.util.List;

@Rule
public class CabRules {
    private CabMaster cabdetail;
    private CabMaster cabtempdetail;
    private RulesMaster rulesdetails;



    @Condition
    public boolean CheckCabs(){
        String rulename = rulesdetails.getRule_name();
        int ruleval = rulesdetails.getRule_value();
        int seat = cabdetail.getCapacity();
//        System.out.println(seat);
        int tripamt = cabdetail.getTrip_amount();
        cabtempdetail=cabdetail;
        cabdetail=null;
        boolean typeval = false;
        if(rulename.equals("TRIP FAIR")){
            if(ruleval == tripamt){
                typeval = true;
            }
        }else if(rulename.equals("CAPACITY")){
            if(ruleval == seat){
                typeval = true;
            }
        }
        return typeval;
    }

    @Action
    public void SeatAvail(){
        setCabdetail(cabtempdetail);
    }


    public CabMaster getCabdetail() {
        return cabdetail;
    }

    public void setCabdetail(CabMaster cabdetail) {
        this.cabdetail = cabdetail;
    }

    public RulesMaster getRulesdetails() {
        return rulesdetails;
    }

    public void setRulesdetails(RulesMaster rulesdetails) {
        this.rulesdetails = rulesdetails;
    }
}
