package com.concretepage.service;

import java.util.List;

import com.concretepage.entity.*;

public interface IArticleService {
     List<Article> getAllArticles();
     List<CabMaster> StartRuleEngine();
     List<RulesMaster> GetRulesMaster();
     Article getArticleById(int articleId);
     boolean createArticle(Article article);
     void updateArticle(Article article);
     void deleteArticle(int articleId);
     boolean login(Greeting greeting);
//     boolean easyrules(CabMaster cabmaster);
     List<Greeting> getAllUsers();
     boolean Login_Validation(Login_Validation logv);

     boolean easyrules(CabMaster cabmaster);
     boolean rulesmaster(RulesMaster rulesmaster);
}
