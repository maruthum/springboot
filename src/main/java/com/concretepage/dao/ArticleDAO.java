package com.concretepage.dao;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.concretepage.entity.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class ArticleDAO implements IArticleDAO {
	@PersistenceContext	
	private EntityManager entityManager;	
	@Override
	public Article getArticleById(int articleId) {
		return entityManager.find(Article.class, articleId);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Article> getAllArticles() {
		String hql = "FROM Article as atcl ORDER BY atcl.articleId DESC";
		return (List<Article>) entityManager.createQuery(hql).getResultList();
	}
	@Override
	public List<CabMaster> StartRuleEngine() {
		String hql = "FROM CabMaster as cms ORDER BY cms.cab_id DESC";
		return (List<CabMaster>) entityManager.createQuery(hql).getResultList();
	}
	@Override
	public List<RulesMaster> GetRulesMaster() {
		String hql = "FROM RulesMaster as rlms WHERE rlms.active = 1";
		return (List<RulesMaster>) entityManager.createQuery(hql).getResultList();
	}
	@Override
	public void createArticle(Article article) {
		entityManager.persist(article);
	}
	@Override
	public void updateArticle(Article article) {
		Article artcl = getArticleById(article.getArticleId());
		artcl.setTitle(article.getTitle());
		artcl.setCategory(article.getCategory());
		entityManager.flush();
	}
	@Override
	public void deleteArticle(int articleId) {
		entityManager.remove(getArticleById(articleId));
	}
	@Override
	public boolean articleExists(String title, String category) {
		String hql = "FROM Article as atcl WHERE atcl.title = ? and atcl.category = ?";
		int count = entityManager.createQuery(hql).setParameter(1, title)
		              .setParameter(2, category).getResultList().size();
		return count > 0 ? true : false;
		
	}
	@Override
	public void login(Greeting greeting) {
		// TODO Auto-generated method stub
		System.out.println("Dao called"+greeting.getEmail());
		System.out.println("Dao Called"+greeting.getPassword());
		entityManager.persist(greeting);
		//String hql = "FROM Article as atcl ORDER BY atcl.articleId DESC";
		//return (List<Article>) entityManager.createQuery(hql).getResultList();		
	}
	@Override
	public void easyrules(CabMaster cabmaster){
		// TODO Auto-generated method stub
		System.out.println("Dao called"+cabmaster.getCab_no());
		System.out.println("Dao Called"+cabmaster.getCab_model());
		System.out.println("Dao Called"+cabmaster.getCapacity());
		System.out.println("Dao Called"+cabmaster.getTariff_type());
		System.out.println("Dao Called"+cabmaster.getTrip_amount());
		System.out.println("Dao Called"+cabmaster.getIn_time());
		System.out.println("Dao Called"+cabmaster.getOut_time());
		entityManager.persist(cabmaster);
		//String hql = "FROM Article as atcl ORDER BY atcl.articleId DESC";
		//return (List<Article>) entityManager.createQuery(hql).getResultList();
	}
	@Override
	public void rulesmaster(RulesMaster rulesmaster){
		// TODO Auto-generated method stub
		System.out.println("Dao called"+rulesmaster.getRule_name());
		System.out.println("Dao Called"+rulesmaster.getRule_value());
		System.out.println("Dao Called"+rulesmaster.getRule_category());
		System.out.println("Dao Called"+rulesmaster.getPreference());
		entityManager.persist(rulesmaster);
		//String hql = "FROM Article as atcl ORDER BY atcl.articleId DESC";
		//return (List<Article>) entityManager.createQuery(hql).getResultList();
	}
	@Override
	public List<Greeting> getAllUsers() {
		// TODO Auto-generated method stub
		String hql = "FROM Greeting as L ORDER BY L.login_id DESC";
		return (List<Greeting>) entityManager.createQuery(hql).getResultList();
	}
	@Override
	public boolean Login_Validation(Login_Validation logv) {
		// TODO Auto-generated method stub
		System.out.println("Dao called Email "+logv.getEmail());
		System.out.println("Dao Called Pass "+logv.getPassword());
		return true;
	}
}
