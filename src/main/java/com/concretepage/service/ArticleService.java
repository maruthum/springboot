package com.concretepage.service;

import java.util.List;

import com.concretepage.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.concretepage.dao.IArticleDAO;

@Service
public class ArticleService implements IArticleService {
	@Autowired
	private IArticleDAO articleDAO;
	@Override
	public Article getArticleById(int articleId) {
		Article obj = articleDAO.getArticleById(articleId);
		return obj;
	}

	@Override
	public List<Article> getAllArticles(){
		return articleDAO.getAllArticles();
	}

	@Override
	public List<CabMaster> StartRuleEngine(){
		return articleDAO.StartRuleEngine();
	}

	@Override
	public List<RulesMaster> GetRulesMaster(){
		return articleDAO.GetRulesMaster();
	}
	@Override
	public synchronized boolean createArticle(Article article){
       if (articleDAO.articleExists(article.getTitle(), article.getCategory())) {
    	   return false;
       } else {
    	   articleDAO.createArticle(article);
    	   return true;
       }
	}
	@Override
	public void updateArticle(Article article) {
		articleDAO.updateArticle(article);
	}
	@Override
	public void deleteArticle(int articleId) {
		articleDAO.deleteArticle(articleId);
	}
	@Override
	public boolean login(Greeting greeting) {
		// TODO Auto-generated method stub
		System.out.println("services called"+greeting);
		 articleDAO.login(greeting);
		 return true;
	}
	@Override
	public boolean easyrules(CabMaster cabmaster) {
		// TODO Auto-generated method stub
		System.out.println("services called"+cabmaster);
		 articleDAO.easyrules(cabmaster);
		 return true;
	}

	@Override
	public boolean rulesmaster(RulesMaster rulesmaster) {
		System.out.println("services called"+rulesmaster);
		articleDAO.rulesmaster(rulesmaster);
		return true;
	}

	@Override
	public List<Greeting> getAllUsers() {
		// TODO Auto-generated method stub
		return articleDAO.getAllUsers();
	}
	@Override
	public boolean Login_Validation(Login_Validation logv) {
		// TODO Auto-generated method stub
	return	articleDAO.Login_Validation(logv);
		//return false;
	}
}
