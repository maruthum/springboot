package com.concretepage.entity;

import javax.persistence.*;

@Entity
@Table(name= "cabmaster")
public class CabMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cab_id")
    private int cab_id;
    @Column(name = "cab_no")
    private String cab_no;
    @Column(name = "cab_model")
    private String cab_model;
    @Column(name = "capacity")
    private int capacity;
    @Column(name = "tariff_type")
    private String tariff_type;
    @Column(name = "trip_amount")
    private int trip_amount;
    @Column(name = "in_time")
    private String in_time;
    @Column(name = "out_time")
    private String out_time;

    public int getCab_id() {
        return cab_id;
    }

    public void setCab_id(int cab_id) {
        this.cab_id = cab_id;
    }

    public String getCab_no() {
        return cab_no;
    }

    public void setCab_no(String cab_no) {
        this.cab_no = cab_no;
    }

    public String getCab_model() {
        return cab_model;
    }

    public void setCab_model(String cab_model) {
        this.cab_model = cab_model;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getTariff_type() {
        return tariff_type;
    }

    public void setTariff_type(String tariff_type) {
        this.tariff_type = tariff_type;
    }

    public int getTrip_amount() {
        return trip_amount;
    }

    public void setTrip_amount(int trip_amount) {
        this.trip_amount = trip_amount;
    }

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public String getOut_time() {
        return out_time;
    }

    public void setOut_time(String out_time) {
        this.out_time = out_time;
    }
}
