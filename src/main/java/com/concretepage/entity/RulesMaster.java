package com.concretepage.entity;

import javax.persistence.*;

@Entity
@Table(name= "rulesmaster")
public class RulesMaster {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rule_id")
    private int rule_id;
    @Column(name = "rule_name")
    private String rule_name;
    @Column(name = "rule_value")
    private int rule_value;
    @Column(name = "rule_category")
    private String rule_category;
    @Column(name = "preference")
    private int preference;
    @Column(name = "active")
    private int active;

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getRule_id() {
        return rule_id;
    }

    public void setRule_id(int rule_id) {
        this.rule_id = rule_id;
    }

    public String getRule_name() {
        return rule_name;
    }

    public void setRule_name(String rule_name) {
        this.rule_name = rule_name;
    }

    public int getRule_value() {
        return rule_value;
    }

    public void setRule_value(int rule_value) {
        this.rule_value = rule_value;
    }

    public String getRule_category() {
        return rule_category;
    }

    public void setRule_category(String rule_category) {
        this.rule_category = rule_category;
    }

    public int getPreference() {
        return preference;
    }

    public void setPreference(int preference) {
        this.preference = preference;
    }
}
