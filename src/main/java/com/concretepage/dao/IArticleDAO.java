package com.concretepage.dao;
import java.util.List;

import com.concretepage.entity.*;

public interface IArticleDAO {
    List<Article> getAllArticles();
    List<CabMaster> StartRuleEngine();
    List<RulesMaster> GetRulesMaster();
    Article getArticleById(int articleId);
    void createArticle(Article article);
    void updateArticle(Article article);
    void deleteArticle(int articleId);
    boolean articleExists(String title, String category);
   void login(Greeting greeting);
   void easyrules(CabMaster cabmaster);
   void rulesmaster(RulesMaster rulesmaster);
   List<Greeting> getAllUsers();
   boolean Login_Validation(Login_Validation logv);
}
 